#define _GNU_SOURCE // RTLD_NEXT

#include <openssl/ssl.h>
#include <polarssl/ssl.h>
#include <openssl/err.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <netinet/in.h>
#include <sys/wait.h> // waitpid


#include <dlfcn.h> // dlsym
#include <unistd.h> // getpid/getpgrp
#include <arpa/inet.h> // inet_ntop

#include <sys/types.h> // getaddrinfo, gethostbyname
#include <sys/socket.h> // getaddrinfo, gethostbyname
#include <netdb.h> // getaddrinfo, gethostbyname

#include <gnutls/gnutls.h>
#include <gnutls/x509.h>

#include <nss/ssl.h> //NSS SSL library
#include <nss/hasht.h> //NSS Hash library
#include <nss/sechash.h>

#include "config.h"
#include "sslshim.h"
#include "converge.h"
#include "common.h"

// DEBUG
#include <time.h>
#include <sys/time.h>

// XXX GnuTLS does not check cert validity, even with this enabled.
// This option is for checking things like cert expiration, etc. Any value
// returned by this other than invalid CA and cert ok are returned. In the
// case of those two, we notarize the cert. Probably not a bad idea, depends
// on what you're worried about.
// Hook _init and _fini are in converge.c

typeof(getaddrinfo) *real_getaddrinfo;
typeof(gethostbyname) *real_gethostbyname;
typeof(connect) *real_connect;
typeof(SSL_connect) *real_SSL_connect;
typeof(SSL_do_handshake) *real_SSL_do_handshake;
typeof(SSL_get_verify_result) *real_SSL_get_verify_result;
typeof(SSL_ForceHandshake) *real_SSL_ForceHandshake;

/* This is a PolarSSL Hook */
int ssl_handshake(ssl_context* ssl) {
  typeof(ssl_handshake) *real_polarSSL_handshake;
  char *host = NULL, *port = NULL;
  char fingerprint[EVP_MAX_MD_SIZE + EVP_MAX_MD_SIZE/2];
  int vmode, realret, verification_status;
  x509_crt *cert;
  pid_t *pid = malloc(sizeof(pid_t)), *grp = malloc(sizeof(pid_t));

  real_polarSSL_handshake = dlsym(RTLD_NEXT, "ssl_handshake");

  // We will take care of the verification ourself
  vmode = ssl->authmode;
  ssl->authmode=SSL_VERIFY_NONE;

  realret = (* real_polarSSL_handshake)(ssl);
  ssl->authmode = vmode;

  // Populate all necessary variables
  polarSSL_finalize_hook(pid, grp, &host, &port, &cert, ssl, fingerprint,
			 "[+] Hooked polarSSL_handshake()");

  verification_status = perform_verification(ssl, realret, host, port, fingerprint);

  cleanup(host, port, pid, grp);

  return verification_status;
}

void polarSSL_finalize_hook (pid_t *pid, pid_t *grp, char **host, char **port, x509_crt **cert, ssl_context *ssl, char *fingerprint, const char *success) {
  unsigned int fprint_size;
  unsigned char fprint[1024];
  //const EVP_MD *fprint_type;

  *pid = getpid();
  *grp = getpgrp();

  debug_log(success);
  debug_log(" <%d,%d>\n", *pid, *grp);

  converge_get_host_polarssl(ssl, host,port);
  //*cert = ssl_get_peer_cert(ssl);
  *cert = ssl->key_cert->cert;
  //if (*cert != NULL) {
  //  unsigned char * out[64];
  _sha_to_ascii(ssl->handshake->fin_sha1.buffer,fprint,ssl->handshake->fin_sha1.total);
  // printf("%s",out);
  /*if (!X509_digest(*cert, fprint_type, fprint, &fprint_size)) {
  // TODO We need to figure out how to handle this situation. Return bad cert?
  debug_log("[-] Bad certificate fingerprint\n");
  exit(223);*/
}

int getaddrinfo(const char *nodename, const char *servname,
            const struct addrinfo *hints, struct addrinfo **res) {
  pid_t pid, grp;
  char addr[INET6_ADDRSTRLEN];
  const char *ptr = NULL;
  int ret;
  struct addrinfo *p;
  struct sockaddr_in *tmp;
  struct sockaddr_in6 *tmp6;

  if (!real_getaddrinfo) {
    real_getaddrinfo = dlsym(RTLD_NEXT, "getaddrinfo");
  }

  ret = (*real_getaddrinfo)(nodename, servname, hints, res);

  pid = getpid();
  grp = getpgrp();

  debug_log("\n");
  debug_log("[+] Hooked getaddrinfo() <%d, %d>\n", pid, grp);
  debug_log("\tname: %s\n", nodename);

  for (p = *res; p; p = p->ai_next) {
    switch (p->ai_family) {
      case AF_INET:
        tmp = (struct sockaddr_in *)p->ai_addr;
        ptr = inet_ntop(AF_INET, &tmp->sin_addr, addr, INET_ADDRSTRLEN);
        break;
      case AF_INET6:
        tmp6 = (struct sockaddr_in6 *)p->ai_addr;
        ptr = inet_ntop(AF_INET6, &tmp6->sin6_addr, addr, INET6_ADDRSTRLEN);
        break;
      default:
        ptr = NULL;
        debug_log("[-] Unknown address type <%d> in hooked getaddrinfo(), skipping\n", p->ai_family);
        //exit(221);
    }

    if (ptr) {
      debug_log("\tip: %s\n", addr);

      // Send it to the db
      converge_getaddrinfo(nodename, addr);
    }
  }

  return ret;
}

struct hostent *gethostbyname(const char *name) {
  struct hostent *he;
  struct in_addr **addr_list;
  char addr[INET6_ADDRSTRLEN];
  const char *ptr = NULL;
  pid_t pid, grp;
  int i;

  if (!real_gethostbyname) {
    real_gethostbyname = dlsym(RTLD_NEXT, "gethostbyname");
  }

  he = (*real_gethostbyname)(name);

  pid = getpid();
  grp = getpgrp();

  debug_log("\n");
  debug_log("[+] Hooked gethostbyname() <%d, %d>\n", pid, grp);
  debug_log("\tname: %s\n", name);

  if (!he) {
    debug_log("\tip: NULL\n");
    return he;
  }

  addr_list = (struct in_addr **)he->h_addr_list;

  for (i = 0; addr_list[i]; i++) {
    switch (he->h_addrtype) {
      case AF_INET:
        ptr = inet_ntop(AF_INET, addr_list[i], addr, INET_ADDRSTRLEN);
        break;
      case AF_INET6:
        ptr = inet_ntop(AF_INET6, addr_list[i], addr, INET6_ADDRSTRLEN);
        break;
      default:
        ptr = NULL;
        debug_log("[-] Unknown address type <%d> in hooked gethostbyname(), skipping\n", he->h_addrtype);
        //exit(222);
    }

    if (ptr) {
      debug_log("\tip: %s\n", addr);

      // Send it to the db
      converge_getaddrinfo(name, addr);
    }
  }

  return he;
}

int connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen) {
  unsigned short port;
  pid_t pid, grp;
  struct sockaddr_in *p;
  struct sockaddr_in6 *p6;
  char address[INET6_ADDRSTRLEN];
  const char *ptr = NULL;
  int skip = 0;
  size_t len = 0;
  char link[1024], exe[1024];

  if (!real_connect) {
    real_connect = dlsym(RTLD_NEXT, "connect");
  }

  pid = getpid();
  grp = getpgrp();

  // Fetch the executable name and store it in global_config
  if (global_config->command_name == NULL) {
    snprintf(link, sizeof(link), "/proc/%d/exe", pid);
    if ((len = readlink(link, exe, sizeof(exe)-1)) == -1) {
      debug_log("[-] FATAL: Unable to retrieve executable filepath\n");
      exit(1);
    }
    exe[len] = '\0';
    global_config->command_name = xmalloc(1024);
    snprintf(global_config->command_name, 1024, "%s", exe);
    debug_log("\n[+] Executable filepath: %s\n", global_config->command_name);
  }

  debug_log("\n");
  debug_log("[+] Hooked connect() <%d, %d>\n", pid, grp);
  debug_log("\tfd: %d\n", sockfd);
  switch (addr->sa_family) {
    case AF_INET:
      p = (struct sockaddr_in *)addr;
      ptr = inet_ntop(AF_INET, &p->sin_addr, address, INET_ADDRSTRLEN);
      port = ntohs(p->sin_port);
      break;
    case AF_INET6:
      p6 = (struct sockaddr_in6 *)addr;
      ptr = inet_ntop(AF_INET6, &p6->sin6_addr, address, INET6_ADDRSTRLEN);
      port = ntohs(p6->sin6_port);
      break;
    default: // AF_UNIX, others
      debug_log("[-] Unknown address type <%d> in hooked connect(), skipping\n", addr->sa_family);
      skip = 1;
      //exit(223);
  }

  if (!skip) {
    if (ptr) {
      debug_log("\tip: %s\n", address);
    }

    debug_log("\tport: %u\n", port);

    converge_connect(address, port, sockfd);
  }

  return (*real_connect)(sockfd, addr, addrlen);
}

// OpenSSL

// XXX forks n shit
/*
 * Ok, so how we deal with forks is going to be kinda tricky
 *
 * So, we want all fork'd processes to share fd, which would 
 * break the (pid, fd) mapping. Instead we're going to use a
 * (pid, parent pid, fd) for a key. Essentally, if we look for
 * a fd:
 *  if getpid() == getpgrp()
 *    select pid, fd
 *  else
 *    select parent_pid, fd 
 *      or
 *    select pid, fd
 *
 * The latter case works like this: if we're a child process
 * we can read our parents fd's legally. However, we can also
 * create our own fd's which are legit, but we don't want other
 * child proccess, or the parent process to consider them legit
 *
 * TODO what happens to a child process when the parent process
 * dies. Other corner cases for process termination and creation
 */

// Helper functions
void finalize_hook (pid_t *pid, pid_t *grp, char **host, char **port, X509 **cert,
                    SSL *ssl, char *fingerprint, const char *success) {
  unsigned int fprint_size;
  unsigned char fprint[EVP_MAX_MD_SIZE];
  const EVP_MD *fprint_type;

  *pid = getpid();
  *grp = getpgrp();

  debug_log(success);
  debug_log(" <%d,%d>\n", *pid, *grp);

  converge_get_host_openssl(ssl, host, port);

  *cert = SSL_get_peer_certificate(ssl);

  if (*cert != NULL) {
    fprint_type = EVP_sha1();

    if (!X509_digest(*cert, fprint_type, fprint, &fprint_size)) {
      // TODO We need to figure out how to handle this situation. Return bad cert?
      debug_log("[-] Bad certificate fingerprint\n");
      exit(223);
    } 

    _sha_to_ascii(fprint, fingerprint, fprint_size);
  } else {
    // TODO We need to figure out how to handle this situation. Return bad cert?
    debug_log("[-] No certificate found or no SSL connection was made\n");
  }
}


int determine_return_by_config_policy (char *host, char *port, char *fingerprint) {

  double total = 0.0, succeeded = 0.0, final_ratio = 0.0;
  struct policy *policy;

  policy = get_host_policy(host, global_config);

  if (policy == NULL) {
    policy = get_command_policy(global_config->command_name, global_config);
    debug_log("[+] CONFIG: No host policy found; searching command policies...\n");
  }

  if (policy == NULL) {
    debug_log("[+] No policy found for %s; falling back\n", global_config->command_name);

    if (global_config->cert_pinning) {
      total += 1.0;
      if ((global_config->cert_pinning_status = cert_pin_check(host, port, fingerprint)) == 0) {
        succeeded += 1.0;
      }
    }
  
    if (global_config->convergence) {
      total += 1.0;
      if ((global_config->convergence_status = converge(host, port, fingerprint)) == 0) {
        succeeded += 1.0;
      }
    }
  
    if (global_config->dane) {
      total += 1.0;
      if ((global_config->dane_status = dane_verify(port, host)) == 0) {
        succeeded += 1.0;
      }
    }
  
    // TODO Base the following on the global_config->cert_authority_status variable
    // Note: We may have to assume that CAs check out until we can perform actual verification
    //   in our do_handshake hook
    if (global_config->cert_authority) {
      total += 1.0;
      // Change recommended by Rashmi and Saili
      /* if ((global_config->cert_authority_status = 0) == 0) { */
      if ( global_config->cert_authority_status == 0 ) {
        succeeded += 1.0;
      }
    }

    if (total == 0.0) {
      debug_log("[+] No verification performed as per global policy\n");
      final_ratio = 1.0;
    } else {
      final_ratio = succeeded / total;
    }

    debug_log("[+] VERIFICATION: policy success to failure ratio is %f\n", final_ratio);

    if (final_ratio >= *(global_config->vote)) {
      return X509_V_OK;
    } else {
      return X509_V_ERR_INVALID_CA;
    }
  } else {
    debug_log("[+] %s policy found\n", global_config->command_name);

    if (*(policy->methods->cert_pinning)) {
      if (cert_pin_check(host, port, fingerprint) != 0) {
        total += 1.0;
      } else {
        total += 1.0;
        succeeded += 1.0;
      }
    }
  
    if (*(policy->methods->convergence)) {
      if (converge(host, port, fingerprint) != 0) {
        total += 1.0;
      } else {
        total += 1.0;
        succeeded += 1.0;
      }
    }
  
    if (*(policy->methods->dane)) {
      if (dane_verify(port, host) != 0) {
        total += 1.0;
      } else {
        total += 1.0;
        succeeded += 1.0;
      }
    }
  
    // TODO Base the following on the global_config->cert_authority_status variable
    // Note: We may have to assume that CAs check out until we can perform actual verification
    //   in our do_handshake hook
    if (*(policy->methods->cert_authority)) {
      if (global_config->cert_authority_status) {
        total += 1.0;
      } else {
        total += 1.0;
        succeeded += 1.0;
      }
    }

    if (total == 0.0) {
      final_ratio = 1.0;
    } else {
      final_ratio = succeeded / total;
    }

    debug_log("[+] VERIFICATION: policy success to failure ratio is %f\n", final_ratio);

    if (final_ratio >= *(policy->vote)) {
      return X509_V_OK;
    } else {
      return X509_V_ERR_INVALID_CA;
    }
  }
}

int perform_verification (void *ssl, int realret, char *host, char *port, char *fingerprint) {
  if (ssl == NULL || host == NULL || port == NULL || fingerprint == NULL) {
    debug_log("[-] NULL SSL, host, port, or fingerprint while verifying certificate\n");

    return -1;
  }

  return determine_return_by_config_policy(host, port, fingerprint);
}

void cleanup (char *host, char *port, pid_t *pid, pid_t *grp) {
  if (host != NULL) {
    free(host);
    host = NULL;
  }

  if (port != NULL) {
    free(port);
    port = NULL;
  }

  if (pid != NULL) {
    free(pid);
    pid = NULL;
  }

  if (grp != NULL) {
    free(grp);
    grp = NULL;
  }
}

int SSL_connect (SSL *ssl) {
  typeof(SSL_connect) *real_SSL_connect;
  char *host = NULL, *port = NULL;
  char fingerprint[EVP_MAX_MD_SIZE + EVP_MAX_MD_SIZE/2];
  int verification_status, realret, vmode;
  X509 *cert;
  pid_t *pid = malloc(sizeof(pid_t)), *grp = malloc(sizeof(pid_t));

  real_SSL_connect = dlsym(RTLD_NEXT, "SSL_connect");

  // The following few blocks prevent custom callbacks that are registered with SSL
  //   by the executable we're hooking
  if (!global_config->cert_authority) {
    vmode = ssl->verify_mode;
    ssl->verify_mode=SSL_VERIFY_NONE;
  }

  realret = (*real_SSL_connect)(ssl);

  if (!global_config->cert_authority) {
    ssl->verify_mode = vmode;
  }

  // Normalize SSL_connect()'s return value
  if (realret == 1) {
    debug_log("[+] SSL_connect CA verification succeeded\n");
    global_config->cert_authority_status = 0;
  } else if (realret < 0) {
    debug_log("[-] FATAL: SSL_connect connection failure\n");
    return -1;
  } else {
    debug_log("[+] SSL_connect custom verification failed\n");
    global_config->cert_authority_status = 0;
  }

  // Populate all necessary variables
  finalize_hook(pid, grp, &host, &port, &cert, ssl, fingerprint,
    "[+] Hooked SSL_connect()");

  verification_status = perform_verification(ssl, global_config->cert_authority_status, host, port, fingerprint);

  cleanup(host, port, pid, grp);

  // De-normalize return values back to what SSL_connect() is supposed to return
  if (verification_status == X509_V_OK) {
    return 1;
  } else {
    return -1;
  }
} 

// Hook the SSL handshake function and then pipe the verification through Convergence at the end.
int SSL_do_handshake(SSL *ssl){
  typeof(SSL_do_handshake) *real_SSL_do_handshake;
  char *host = NULL, *port = NULL;
  char fingerprint[EVP_MAX_MD_SIZE + EVP_MAX_MD_SIZE/2];
  int vmode, realret, verification_status;
  X509 *cert;
  pid_t *pid = malloc(sizeof(pid_t)), *grp = malloc(sizeof(pid_t));

  real_SSL_do_handshake = dlsym(RTLD_NEXT, "SSL_do_handshake");

  // We will take care of the verification ourself
  vmode = ssl->verify_mode;
  ssl->verify_mode=SSL_VERIFY_NONE;

  realret = (*real_SSL_do_handshake)(ssl);
  ssl->verify_mode = vmode;

  // Populate all necessary variables
  finalize_hook(pid, grp, &host, &port, &cert, ssl, fingerprint,
    "[+] Hooked SSL_do_handshake()");

  verification_status = perform_verification(ssl, realret, host, port, fingerprint);

  cleanup(host, port, pid, grp);

  return verification_status;
} 

long SSL_get_verify_result(const SSL *const_ssl) {
  char *host = NULL, *port = NULL;
  char fingerprint[EVP_MAX_MD_SIZE + EVP_MAX_MD_SIZE/2];
  int ret, verification_status;
  X509 *cert = NULL;
  pid_t *pid = malloc(sizeof(pid_t)), *grp = malloc(sizeof(pid_t));
  SSL *ssl = malloc(sizeof(SSL));

  // This "undoes" the const constraint on the argument that's passed in
  ssl = memcpy(ssl, const_ssl, sizeof(SSL));

  /*
   * Convergence just makes sure the certificates are the same, it does not
   * check for expiration, revocation, etc. There is some overlap here we
   * don't want (revocation specifically), but it's a good catchall that
   * prevents us from having to check all the other error conditions ourselfs
   */
  if (global_config->cert_authority) {
    if (!real_SSL_get_verify_result) {
      real_SSL_get_verify_result = dlsym(RTLD_NEXT, "SSL_get_verify_result");
    }

    ret = (*real_SSL_get_verify_result)(ssl);

    if (ret != X509_V_OK && ret != X509_V_ERR_INVALID_CA) {
      return ret;
    } else if (ret == X509_V_OK) {
      global_config->cert_authority_status = 0;
    } else {
      global_config->cert_authority_status = 1;
    }
  }

  // Populate all necessary variables
  finalize_hook(pid, grp, &host, &port, &cert, ssl, fingerprint,
    "[+] Hooked SSL_get_verify_result()");

  //verification_status = perform_verification(ssl, ret, host, port, fingerprint);
  verification_status = perform_verification(ssl, 0, host, port, fingerprint);

  cleanup(host, port, pid, grp);

  return verification_status;
}

// GnuTLS

// Not sure if this is present in our current lib version, and is untested
int gnutls_certificate_verify_peers3(gnutls_session_t session, char *host, unsigned int *status) {
  return gnutls_certificate_verify_peers2(session, status);
}

int gnutls_certificate_verify_peers2(gnutls_session_t session, unsigned int *status) {
  pid_t pid, grp;
  cert_auth_info_t info;
  gnutls_x509_crt_t peer_cert;
  int ret, daneret, cpret;
  char *host, *port, fingerprint[MAX_HASH_SIZE*3+1];
  unsigned char fprint[MAX_HASH_SIZE];
  unsigned int fprint_size = MAX_HASH_SIZE;

  pid = getpid();
  grp = getpgrp();

  debug_log("\n");
  debug_log("[+] Hooked gnutls_certificate_verify_peers2() <%d, %d>\n", pid, grp);

  // Hostname is located here if we want it
  // ((server_name_ext_st *)(session->internals.extension_int_data[0].priv.ptr)).server_names.name
  //
  // No port info though as far as I can tell...

  converge_get_host_gnutls(session, &host, &port);

  info = session->key->auth_info;

  if (!info) {
    *status = GNUTLS_E_NO_CERTIFICATE_FOUND;
    return -1;
  }

  ret = gnutls_x509_crt_init(&peer_cert);

  if (ret < 0) {
    debug_log("[-] gnutls_certificate_verify_peers2(): cannot get cert\n");
    exit(224);
  }

  ret = gnutls_x509_crt_import(peer_cert, &info->raw_certificate_list[0], GNUTLS_X509_FMT_DER);

  if (ret < 0) {
    debug_log("[-] gnutls_certificate_verify_peers2(): cannot get cert\n");
    exit(224);
  }

  ret = gnutls_x509_crt_get_fingerprint(peer_cert, GNUTLS_DIG_SHA1, fprint, &fprint_size);

  if (ret < 0) {
    debug_log("[-] gnutls_certificate_verify_peers2(): cannot get SHA1 sum of cert\n");
    exit(224);
  }

  _sha_to_ascii(fprint, fingerprint, fprint_size);


  cpret = 0;
  if (global_config->cert_pinning) {
    cpret = cert_pin_check(host, port, fingerprint);

    if(cpret == -1){
      *status |= GNUTLS_E_CERTIFICATE_ERROR;
      return -1;
    }
  }

  daneret = 0;
  if (global_config->dane) {
    daneret = dane_verify(port, host);
  }
  
  if (global_config->convergence) {
    ret = converge(host, port, fingerprint);
  } else {
    ret = 0;
  }

  free(host);
  free(port);

  // TODO Should ret and daneret be mutually exclusive?
  if (!ret && !daneret) {
    debug_log("[+] converge returned: %d, returning 0 and not setting status\n", ret);
    ret = 0;
  } else {
    debug_log("[+] converge returned: %d, returning -1 and setting status\n", ret);
    *status |= GNUTLS_E_CERTIFICATE_ERROR;
    ret = -1;
  }

  return ret;
}

//Code changes to support NSS

//NSS start - Helper function to get fingerprint, host, port, pid and groupid for the given connection
void initialize_hook(pid_t *pid, pid_t *grp, char **host, char **port, CERTCertificate **cert,
                      PRFileDesc *fd, char *fingerprint, const char *success){
    //TODO fingerprint variables
    unsigned char fprint[SHA1_LENGTH];
    unsigned int fprint_size;
    HASHContext *hashctx;
  
    *pid = getpid();
    *grp = getpgrp();

    debug_log(success);
    debug_log(" <%d,%d>\n", *pid, *grp);

    converge_get_host_nss(fd, host, port);

    *cert = SSL_PeerCertificate(fd);
    //We need to extract the SHA1 fingerprint of the certificate from the CERTcertificate object returned by SSL.
    if(*cert != NULL){
      debug_log("[+] Certificate found\n");

      hashctx = HASH_Create(HASH_AlgSHA1);
      HASH_Begin(hashctx);
      HASH_Update(hashctx, (*cert)->derCert.data, (*cert)->derCert.len);
      HASH_End(hashctx, fprint, &fprint_size, SHA1_LENGTH);
      HASH_Destroy(hashctx);

      _sha_to_ascii(fprint, fingerprint, fprint_size);

    } else {
      // TODO We need to figure out how to handle this situation. Return bad cert?
      debug_log("[-] No certificate found or no SSL connection was made\n");
    }
}

int determine_return_by_config_policy_nss(char *host, char *port, char *fingerprint) {
  double total = 0.0, succeeded = 0.0, final_ratio = 0.0;
  struct policy *policy;

  policy = get_host_policy(host, global_config);

  if (policy == NULL) {
    policy = get_command_policy(global_config->command_name, global_config);
    debug_log("[+] CONFIG: No host policy found; searching command policies...\n");
  }

  if (policy == NULL) {
    debug_log("[+] No policy found for %s; falling back\n", global_config->command_name);

    if (global_config->cert_pinning) {
      total += 1.0;
      if ((global_config->cert_pinning_status = cert_pin_check(host, port, fingerprint)) == 0) {
        succeeded += 1.0;
      }
    }
  
    if (global_config->convergence) {
      total += 1.0;
      if ((global_config->convergence_status = converge(host, port, fingerprint)) == 0) {
        succeeded += 1.0;
      }
    }
  
    if (global_config->dane) {
      total += 1.0;
      if ((global_config->dane_status = dane_verify(port, host)) == 0) {
        succeeded += 1.0;
      }
    }

    if (global_config->cert_authority) {
      total += 1.0;
      if (global_config->cert_authority_status == 0) {
        succeeded += 1.0;
      }
    }

    if (total == 0.0) {
      debug_log("[+] No verification performed as per global policy\n");
      final_ratio = 1.0;
    } else {
      final_ratio = succeeded / total;
    }

    debug_log("[+] VERIFICATION: policy success to failure ratio is %f\n", final_ratio);

    if (final_ratio >= *(global_config->vote)) {
      return SECSuccess;
    } else {
      return SECFailure;
    }
  } else {
    debug_log("[+] %s policy found\n", global_config->command_name);

    if (*(policy->methods->cert_pinning)) {
      if (cert_pin_check(host, port, fingerprint) != 0) {
        total += 1.0;
      } else {
        total += 1.0;
        succeeded += 1.0;
      }
    }
  
    if (*(policy->methods->convergence)) {
      if (converge(host, port, fingerprint) != 0) {
        total += 1.0;
      } else {
        total += 1.0;
        succeeded += 1.0;
      }
    }
  
    if (*(policy->methods->dane)) {
      if (dane_verify(port, host) != 0) {
        total += 1.0;
      } else {
        total += 1.0;
        succeeded += 1.0;
      }
    }
  
    // TODO Base the following on the global_config->cert_authority_status variable
    // Note: We may have to assume that CAs check out until we can perform actual verification
    //   in our do_handshake hook
    if (*(policy->methods->cert_authority)) {
      if (global_config->cert_authority_status) {
        total += 1.0;
      } else {
        total += 1.0;
        succeeded += 1.0;
      }
    }

    if (total == 0.0) {
      final_ratio = 1.0;
    } else {
      final_ratio = succeeded / total;
    }

    debug_log("[+] VERIFICATION: policy success to failure ratio is %f\n", final_ratio);

    if (final_ratio >= *(policy->vote)) {
      return SECSuccess;
    } else {
      return SECFailure;
    }
  }
}

//Hook for actual NSS SSL verfication function
SECStatus SSL_ForceHandshake(PRFileDesc *fd){
  typeof(SSL_ForceHandshake) *real_SSL_ForceHandshake;
  char *host = NULL, *port = NULL;
  char fingerprint[HASH_LENGTH_MAX + HASH_LENGTH_MAX/2]; //need to figure the size of this
  SECStatus real_return, verification_status;
  CERTCertificate *cert;
  pid_t *pid = malloc(sizeof(pid_t)), *grp= malloc(sizeof(pid_t));

  real_SSL_ForceHandshake = dlsym(RTLD_NEXT,"SSL_ForceHandshake");

  debug_log("CA authorization enabled\n");
  real_return = (*real_SSL_ForceHandshake)(fd);
  //printf("%d\n",real_return);  
    
  if(real_return == SECSuccess){
    global_config->cert_authority_status = 0;
  } else {
    global_config->cert_authority_status = 1;
  }

  initialize_hook(pid, grp, &host, &port, &cert, fd, fingerprint,"[+] Hooked SSL_ForceHandshake");

  verification_status = determine_return_by_config_policy_nss(host, port, fingerprint);

  cleanup(host, port, pid, grp);

  debug_log("[+] Hooked SSL_ForceHandshake\n");

  return verification_status;
}
//NSS end

void _sha_to_ascii(const unsigned char *in, char *out, unsigned int size) {
  int i;
  for (i = 0; i < size - 1; i++) {
    snprintf(&out[i*3], 4,"%02X:", in[i]);
  }
  snprintf(&out[i*3], 4, "%02X", in[i]);
}
