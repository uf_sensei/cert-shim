#ifndef _COMMONH
#define _COMMONH

void debug_log(const char *fmt, ...);
void *xmalloc(unsigned int bytes);

#endif
