#!/usr/bin/python
import os

# Outputs ssldependency.txt, which is a quick look at what may have SSL
# Outputs relevant dependencies per package to STDOUT, may want to pipe to file

file1 = open('ubuntuTop10000.txt', "r")
file2 = open('ssldependency.txt', "w")

for line in file1:
	tokens = line.split()
	request = "apt-rdepends " + tokens[1] + " > results.txt"
	os.system(request)
	returncode = os.system("grep -i 'ssl\|libnss\|crypt\|gnutls' results.txt >> junk.txt")
	if returncode != 0:
		data = "No " + tokens[1] + " does not have any ssl dependencies\n"
		file2.write(data)
	else:
		data = "Yes " + tokens[1] + " may have ssl dependencies.\n"
		file2.write(data)
		request2 = "echo " + tokens[1] + " --------------------------"
		os.system(request2)
		os.system("echo RELEVANT DEPENDENCIES:")
		returncode = os.system("grep -i 'ssl\|libnss\|crypt\|gnutls' results.txt")
		os.system("echo -----------------------------------------")

file1.close()
file2.close()

os.system("rm junk.txt")
os.system("rm results.txt")
