#!/usr/bin/python
import os

# Takes all the packages in yesssldependency.txt and attempts to download source
# Deletes all .gz and .tar files in folder, use with caution

# Make sure you have a file of all yes's
file1 = open('yesssldependency.txt', "r")

# This stuff is for incremental grabbing of source
counter = 0
minimum = 0
maximum = 738


for line in file1:
	counter = counter + 1
	if (counter > minimum and counter <= maximum):
		tokens = line.split()
		# Lets hope no one has a package name that does command injection :-/ yolo
		request = "apt-get source " + tokens[1]
		os.system(request)


# Remove zips and tars, check yourself
os.system("rm *.gz*")
os.system("rm *.tar*")

