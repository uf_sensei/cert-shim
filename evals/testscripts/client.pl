    use strict;
    use IO::Socket::SSL;

    # THIS PROBABLY DOES NOT WORK

    # simple HTTP client -----------------------------------------------
    my $client = IO::Socket::SSL->new(
        # where to connect
        #PeerHost => "www.google.com",
        PeerHost => "www.torproject.org",
        PeerPort => "https",

        # certificate verification
        SSL_verify_mode => SSL_VERIFY_PEER,
        #SSL_verify_mode => SSL_VERIFY_NONE,
        SSL_ca_path => '/etc/ssl/certs', # typical CA path on Linux
        # on OpenBSD instead: SSL_ca_file => '/etc/ssl/cert.pem'


    ) or die "failed connect or ssl handshake: $!,$SSL_ERROR";

    # send and receive over SSL connection
    print $client "GET / HTTP/1.0\r\n\r\n";
    print <$client>;
