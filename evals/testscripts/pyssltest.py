#!/usr/bin/python

import socket, ssl, sys, time

usage = """USAGE: python newSSLtest.py [good||bad]
Where good or bad represents the type of cert to hit."""

if len(sys.argv) == 1:
	print "%s" % usage
	exit()

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# require a certificate from the server
ssl_sock = ssl.wrap_socket(s, cert_reqs=ssl.CERT_REQUIRED, ca_certs="cacert.pem")

# Has a good cert
if sys.argv[1:] == ['good']:
	print "Establishing connection to good cert at systems.cs.uoregon.edu"
	ssl_sock.connect(('torproject.org', 443))
# Has a bad cert
elif sys.argv[1:] == ['bad']:
	print "Establishing connection to bad cert at joepletcher.com"
	ssl_sock.connect(('joepletcher.com', 443))
else:
	print "Unrecognized option %s" % sys.argv[1:]
	print "%s" % usage
	exit()


ssl_sock.write("""GET / HTTP/1.0\r
Host: systems.cs.uoregon.edu\r\n\r\n""")
# Read a chunk of data.  Will not necessarily
# read all the data returned by the server.
data = ssl_sock.read()

print "-----------------------------------------------------"
print data
# note that closing the SSLSocket will also close the underlying socket
ssl_sock.close()
