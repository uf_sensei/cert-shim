# -*- coding: latin-1 -*-
#
# Copyright (C) AB Strakt
# Copyright (C) Jean-Paul Calderone
# See LICENSE for details.

"""
Simple SSL client, using blocking I/O
"""

from OpenSSL import SSL
import sys, os, select, socket

def verify_cb(conn, cert, errnum, depth, ok):
    # This obviously has to be updated
    print 'Got certificate: %s' % cert.get_subject()
    if not ok:
        print "Bad Certs"
    else:
        print "Certs are fine"
    return ok

if len(sys.argv) < 3:
    print 'Usage: python[2] client.py HOST PORT'
    sys.exit(1)

dir = os.path.dirname(sys.argv[0])
if dir == '':
    dir = os.curdir

# Initialize context
ctx = SSL.Context(SSL.SSLv23_METHOD)
ctx.set_verify(SSL.VERIFY_PEER | SSL.VERIFY_FAIL_IF_NO_PEER_CERT, verify_cb) # Demand a certificate
ctx.load_verify_locations(os.path.join(dir, 'cacert.pem'))

# Set up client
sock = SSL.Connection(ctx, socket.socket(socket.AF_INET, socket.SOCK_STREAM))
sock.connect((sys.argv[1], int(sys.argv[2])))

sock.write("""GET / HTTP/1.0\r
Host: systems.cs.uoregon.edu\r\n\r\n""")
# Read a chunk of data.  Will not necessarily
# read all the data returned by the server.
data = sock.read(1024)

print "-----------------------------------------------------"
print data
"""
try:
	sock.send("GET")
        sys.stdout.write(sock.recv(1024))
        sys.stdout.flush()
except SSL.Error as e:
        print 'Connection died unexpectedly'
        print e
"""


sock.shutdown()
sock.close()
