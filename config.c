#include <libconfig.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#include "common.h"

// Prototypes for functions that are intended for internal use only
void check_fatal_condition (int, const char *);
void fetch_boolean_config_option (config_t, int *, const char *, const char *, const char *, const char *);
int fetch_command_policies (config_t, struct certshim_config *);
int fetch_host_policies (config_t, struct certshim_config *);

// Atomic helper functions
void check_fatal_condition (int fail, const char *message) {
  if (fail) {
    debug_log(message);
    exit(1);
  }
}

void fetch_boolean_config_option (config_t cfg, int *config_member, const char *success_activated,
                                  const char *success_disabled, const char *failure, const char *path) {
  int *value = malloc(sizeof(int));

  check_fatal_condition(value == NULL,
    "[-] CONFIG FATAL: malloc() failure while parsing configuration; exiting\n");

  if (config_lookup_bool(&cfg, path, value) == CONFIG_TRUE) {
    if (*value == 1) {
      debug_log(success_activated);
      *config_member = 1;
    } else {
      debug_log(success_disabled);
      *config_member = 0;
    }
  } else {
    debug_log(failure);
    *config_member = 0;
  }

  free(value);
}

int fetch_host_policies (config_t cfg, struct certshim_config *config) {
  int nPolicies = 0;
  unsigned int index;
  config_setting_t *host_policies = config_lookup(&cfg, "host_policies");
  config_setting_t *policy, *methods;
  struct policy *certshim_policy, *head, *previous;
  char *host;

  if (host_policies == NULL) {
    debug_log("[+] CONFIG: No host policies detected\n");

    return 0;
  }

  nPolicies = config_setting_length(host_policies);
  for (index = 0; index < nPolicies; index++) {
    policy = config_setting_get_elem(host_policies, index);

    if (policy == NULL) {
      debug_log("[-] Bad host policy at index %d; skipping\n", index);

      continue;
    }

    // With policy in hand, allocate new policy struct
    certshim_policy = xmalloc(sizeof(struct policy));
    certshim_policy->host = xmalloc(1024);

    // Parse and set the hostname against which the policy will be enforced
    if (config_setting_lookup_string(policy, "host", &host) == CONFIG_TRUE) {
      snprintf(certshim_policy->host, 1024, "%s", host);
      debug_log("[+] POLICY: Host %s policy recognized\n", certshim_policy->host);
    } else {
      debug_log("[-] POLICY: Failed to recognize hostname at host policy index %d; skipping\n", index);
      free(certshim_policy->host);
      free(certshim_policy);

      continue;
    }

    // Parse and set the vote requirement, if available. Else, use fallback.
    certshim_policy->vote = xmalloc(sizeof(double));
    if (config_setting_lookup_float(policy, "vote", certshim_policy->vote) == CONFIG_TRUE) {
      debug_log("[+] POLICY: %s policy vote requirement of >= %f set\n",
        certshim_policy->host, *(certshim_policy->vote));
    } else {
      debug_log("[-] POLICY: Failed to recognize vote ratio at host policy index %d; default to 1.00\n", index);
      *(certshim_policy->vote) = 1.00;
    }

    // Parse per-command verification methods
    methods = config_setting_get_member(policy, "methods");

    if (methods == NULL) {
      debug_log("[+] %s policy: no methods found; falling back to defaults\n");
      *(certshim_policy->methods->cert_pinning) = config->cert_pinning;
      *(certshim_policy->methods->cert_authority) = config->cert_authority;
      *(certshim_policy->methods->convergence) = config->convergence;
      *(certshim_policy->methods->dane) = config->dane;

      return 0;
    }

    certshim_policy->methods = xmalloc(sizeof(struct policy));

    // TODO The following block of code is repeated. Fix it.
    // Check policy cert_pinning configuration
    certshim_policy->methods->cert_pinning = xmalloc(sizeof(int));
    if (config_setting_lookup_bool(methods, "cert_pinning", certshim_policy->methods->cert_pinning) == CONFIG_TRUE) {
      if (*(certshim_policy->methods->cert_pinning) == 1) {
        debug_log("[+] POLICY: %s certificate pinning enforced (%d)\n",
          certshim_policy->host, *(certshim_policy->methods->cert_pinning));
      } else {
        *(certshim_policy->methods->cert_pinning) = 0;
        debug_log("[+] POLICY: %s certificate pinning disabled (%d)\n",
          certshim_policy->host, *(certshim_policy->methods->cert_pinning));
      }
    } else {
      *(certshim_policy->methods->cert_pinning) = config->cert_pinning;
      debug_log("[+] POLICY: %s fallback to default certificate pinning configuration (%d)\n",
        certshim_policy->host, *(certshim_policy->methods->cert_pinning));
    }

    // Check policy certificate authority configuration
    certshim_policy->methods->cert_authority = xmalloc(sizeof(int));
    if (config_setting_lookup_bool(methods, "cert_authority", certshim_policy->methods->cert_authority) == CONFIG_TRUE) {
      if (*(certshim_policy->methods->cert_authority) == 1) {
        debug_log("[+] POLICY: %s certificate authority enforced (%d)\n",
          certshim_policy->host, *(certshim_policy->methods->cert_authority));
      } else {
        *(certshim_policy->methods->cert_authority) = 0;
        debug_log("[+] POLICY: %s certificate authority disabled (%d)\n",
          certshim_policy->host, *(certshim_policy->methods->cert_authority));
      }
    } else {
      *(certshim_policy->methods->cert_authority) = config->cert_authority;
      debug_log("[+] POLICY: %s fallback to default certificate pinning configuration (%d)\n",
        certshim_policy->host, *(certshim_policy->methods->cert_authority));
    }

    // Check policy convergence configuration
    certshim_policy->methods->convergence = xmalloc(sizeof(int));
    if (config_setting_lookup_bool(methods, "convergence", certshim_policy->methods->convergence) == CONFIG_TRUE) {
      if (*(certshim_policy->methods->convergence) == 1) {
        debug_log("[+] POLICY: %s convergence enforced (%d)\n",
          certshim_policy->host, *(certshim_policy->methods->convergence));
      } else {
        *(certshim_policy->methods->convergence) = 0;
        debug_log("[+] POLICY: %s convergence disabled (%d)\n",
          certshim_policy->host, *(certshim_policy->methods->convergence));
      }
    } else {
      *(certshim_policy->methods->convergence) = config->convergence;
      debug_log("[+] POLICY: %s fallback to default convergence configuration (%d)\n",
        certshim_policy->host, *(certshim_policy->methods->convergence));
    }

    // Check policy dane configuration
    certshim_policy->methods->dane = xmalloc(sizeof(int));
    if (config_setting_lookup_bool(methods, "dane", certshim_policy->methods->dane) == CONFIG_TRUE) {
      if (*(certshim_policy->methods->dane) == 1) {
        debug_log("[+] POLICY: %s dane enforced (%d)\n",
          certshim_policy->host, *(certshim_policy->methods->dane));
      } else {
        *(certshim_policy->methods->dane) = 0;
        debug_log("[+] POLICY: %s dane disabled (%d)\n",
          certshim_policy->host, *(certshim_policy->methods->dane));
      }
    } else {
      *(certshim_policy->methods->dane) = config->dane;
      debug_log("[+] POLICY: %s fallback to default dane configuration (%d)\n",
        certshim_policy->host, *(certshim_policy->methods->dane));
    }

    // If this is the first policy, set it as the head of the list
    //   else, add it to the end of the list
    if (index == 0) {
      certshim_policy->previous = certshim_policy;
      certshim_policy->next = certshim_policy;
      certshim_policy->head = certshim_policy;
      head = certshim_policy;
      previous = certshim_policy;

      config->host_policies = head;

      config->n_host_policies = 1;
    } else {
      previous->next = certshim_policy;
      certshim_policy->previous = previous;
      certshim_policy->next = head;
      certshim_policy->head = head;
      previous = certshim_policy;

      config->n_host_policies += 1;
    }
  }

  debug_log("[+] %d policies handled\n", config->n_host_policies);

  return 0;
}

int fetch_command_policies (config_t cfg, struct certshim_config *config) {
  int nPolicies = 0;
  unsigned int index;
  config_setting_t *cmd_policies = config_lookup(&cfg, "command_policies");
  config_setting_t *policy, *methods;
  struct policy *certshim_policy, *head, *previous;
  char *cmd;

  if (cmd_policies == NULL) {
    debug_log("[+] CONFIG: No command policies detected\n");

    return 0;
  }

  nPolicies = config_setting_length(cmd_policies);
  for (index = 0; index < nPolicies; index++) {
    policy = config_setting_get_elem(cmd_policies, index);

    if (policy == NULL) {
      debug_log("[-] Bad command policy at index %d; skipping\n", index);

      continue;
    }

    // With policy in hand, allocate new policy struct
    certshim_policy = xmalloc(sizeof(struct policy));
    certshim_policy->command = xmalloc(1024);

    // Parse and set the command name against which the policy will be enforced
    if (config_setting_lookup_string(policy, "cmd", &cmd) == CONFIG_TRUE) {
      snprintf(certshim_policy->command, 1024, "%s", cmd);
      debug_log("[+] POLICY: Command %s policy recognized\n", certshim_policy->command);
    } else {
      debug_log("[-] POLICY: Failed to recognize command at command policy index %d; skipping\n", index);
      free(certshim_policy->command);
      free(certshim_policy);

      continue;
    }

    // Parse and set the vote requirement, if available. Else, use fallback.
    certshim_policy->vote = xmalloc(sizeof(double));
    if (config_setting_lookup_float(policy, "vote", certshim_policy->vote) == CONFIG_TRUE) {
      debug_log("[+] POLICY: %s policy vote requirement of >= %f set\n",
        certshim_policy->command, *(certshim_policy->vote));
    } else {
      debug_log("[-] POLICY: Failed to recognize command at command policy index %d; default to 1.00\n", index);
      *(certshim_policy->vote) = 1.00;
    }

    // Parse per-command verification methods
    methods = config_setting_get_member(policy, "methods");

    if (methods == NULL) {
      debug_log("[+] %s policy: no methods found; falling back to defaults\n");
      *(certshim_policy->methods->cert_pinning) = config->cert_pinning;
      *(certshim_policy->methods->cert_authority) = config->cert_authority;
      *(certshim_policy->methods->convergence) = config->convergence;
      *(certshim_policy->methods->dane) = config->dane;

      return 0;
    }

    certshim_policy->methods = xmalloc(sizeof(struct policy));

    // TODO The following block of code is repeated. Fix it.
    // Check policy cert_pinning configuration
    certshim_policy->methods->cert_pinning = xmalloc(sizeof(int));
    if (config_setting_lookup_bool(methods, "cert_pinning", certshim_policy->methods->cert_pinning) == CONFIG_TRUE) {
      if (*(certshim_policy->methods->cert_pinning) == 1) {
        debug_log("[+] POLICY: %s certificate pinning enforced (%d)\n",
          certshim_policy->command, *(certshim_policy->methods->cert_pinning));
      } else {
        *(certshim_policy->methods->cert_pinning) = 0;
        debug_log("[+] POLICY: %s certificate pinning disabled (%d)\n",
          certshim_policy->command, *(certshim_policy->methods->cert_pinning));
      }
    } else {
      *(certshim_policy->methods->cert_pinning) = config->cert_pinning;
      debug_log("[+] POLICY: %s fallback to default certificate pinning configuration (%d)\n",
        certshim_policy->command, *(certshim_policy->methods->cert_pinning));
    }

    // Check policy certificate authority configuration
    certshim_policy->methods->cert_authority = xmalloc(sizeof(int));
    if (config_setting_lookup_bool(methods, "cert_authority", certshim_policy->methods->cert_authority) == CONFIG_TRUE) {
      if (*(certshim_policy->methods->cert_authority) == 1) {
        debug_log("[+] POLICY: %s certificate authority enforced (%d)\n",
          certshim_policy->command, *(certshim_policy->methods->cert_authority));
      } else {
        *(certshim_policy->methods->cert_authority) = 0;
        debug_log("[+] POLICY: %s certificate authority disabled (%d)\n",
          certshim_policy->command, *(certshim_policy->methods->cert_authority));
      }
    } else {
      *(certshim_policy->methods->cert_authority) = config->cert_authority;
      debug_log("[+] POLICY: %s fallback to default certificate pinning configuration (%d)\n",
        certshim_policy->command, *(certshim_policy->methods->cert_authority));
    }

    // Check policy convergence configuration
    certshim_policy->methods->convergence = xmalloc(sizeof(int));
    if (config_setting_lookup_bool(methods, "convergence", certshim_policy->methods->convergence) == CONFIG_TRUE) {
      if (*(certshim_policy->methods->convergence) == 1) {
        debug_log("[+] POLICY: %s convergence enforced (%d)\n",
          certshim_policy->command, *(certshim_policy->methods->convergence));
      } else {
        *(certshim_policy->methods->convergence) = 0;
        debug_log("[+] POLICY: %s convergence disabled (%d)\n",
          certshim_policy->command, *(certshim_policy->methods->convergence));
      }
    } else {
      *(certshim_policy->methods->convergence) = config->convergence;
      debug_log("[+] POLICY: %s fallback to default convergence configuration (%d)\n",
        certshim_policy->command, *(certshim_policy->methods->convergence));
    }

    // Check policy dane configuration
    certshim_policy->methods->dane = xmalloc(sizeof(int));
    if (config_setting_lookup_bool(methods, "dane", certshim_policy->methods->dane) == CONFIG_TRUE) {
      if (*(certshim_policy->methods->dane) == 1) {
        debug_log("[+] POLICY: %s dane enforced (%d)\n",
          certshim_policy->command, *(certshim_policy->methods->dane));
      } else {
        *(certshim_policy->methods->dane) = 0;
        debug_log("[+] POLICY: %s dane disabled (%d)\n",
          certshim_policy->command, *(certshim_policy->methods->dane));
      }
    } else {
      *(certshim_policy->methods->dane) = config->dane;
      debug_log("[+] POLICY: %s fallback to default dane configuration (%d)\n",
        certshim_policy->command, *(certshim_policy->methods->dane));
    }

    // If this is the first policy, set it as the head of the list
    //   else, add it to the end of the list
    if (index == 0) {
      certshim_policy->previous = certshim_policy;
      certshim_policy->next = certshim_policy;
      certshim_policy->head = certshim_policy;
      head = certshim_policy;
      previous = certshim_policy;

      config->command_policies = head;

      config->n_command_policies = 1;
    } else {
      previous->next = certshim_policy;
      certshim_policy->previous = previous;
      certshim_policy->next = head;
      certshim_policy->head = head;
      previous = certshim_policy;

      config->n_command_policies += 1;
    }
  }

  debug_log("[+] %d policies handled\n", config->n_command_policies);

  return 0;
}

// External functions
void destroy_config (struct certshim_config *config) {
  // TODO Free all the policy structs and host_policy structs
  if (config != NULL) {
    free(config);
  }
}

// Returns NULL if no policy matches the requested command
struct policy *get_command_policy (const char *cmd, struct certshim_config *config) {
  struct policy *current_policy;

  if (config->n_command_policies <= 0) {
    return NULL;
  }

  current_policy = config->command_policies;

  do {
    if (strcmp(cmd, current_policy->command) == 0) {
      return current_policy;
    }

    current_policy = current_policy->next;
  } while (current_policy != config->command_policies);

  return NULL;
}

// Returns NULL if no policy matches the requested command
struct policy *get_host_policy (const char *host, struct certshim_config *config) {
  struct policy *current_policy;

  if (config->n_host_policies <= 0) {
    return NULL;
  }

  current_policy = config->host_policies;

  do {
    if (strcmp(host, current_policy->host) == 0) {
      return current_policy;
    }

    current_policy = current_policy->next;
  } while (current_policy != config->host_policies);

  return NULL;
}

int converge_parse_config (struct certshim_config *config) {
  config_t cfg;


  char * cfg_path = getenv("CERTSHIM_CONFIG");

  // Initialize libconfig configuration struct
  config_init(&cfg);

  // Parse config file; bail out on failure
  if (!config_read_file(&cfg, cfg_path)) {
    debug_log("[-] CONFIG FATAL: Unable to read configuration file (Line %d: %s)\n",
              config_error_line(&cfg), config_error_text(&cfg));

    config_destroy(&cfg);

    return 1;
  }

  // Parse and save configuration options
  fetch_boolean_config_option(
    cfg,
    &(config->cert_pinning),
    "[+] CONFIG: Certificate pinning enabled\n",
    "[+] CONFIG: Certificate pinning disabled\n",
    "[-] CONFIG: Unable to read cert_pinning option; assume disabled\n",
    "verification_methods.cert_pinning"
  );

  fetch_boolean_config_option(
    cfg,
    &(config->convergence),
    "[+] CONFIG: Convergence enabled\n",
    "[+] CONFIG: Convergence disabled\n",
    "[-] CONFIG: Unable to read convergence option; assume disabled\n",
    "verification_methods.convergence"
  );

  fetch_boolean_config_option(
    cfg,
    &(config->dane),
    "[+] CONFIG: DANE enabled\n",
    "[+] CONFIG: DANE disabled\n",
    "[-] CONFIG: Unable to read dane option; assume disabled\n",
    "verification_methods.dane"
  );

  fetch_boolean_config_option(
    cfg,
    &(config->cert_authority),
    "[+] CONFIG: CA verification enabled\n",
    "[+] CONFIG: CA verification disabled\n",
    "[-] CONFIG: Unable to read CA option; assume disabled\n",
    "verification_methods.cert_authority"
  );

  // Fetch the global vote requirement; default to 1.00 if not found
  config->vote = xmalloc(sizeof(double));
  if (config_lookup_float(&cfg, "verification_methods.vote", config->vote) == CONFIG_TRUE) {
    debug_log("[+] POLICY: Global policy vote requirement of >= %f set\n", *(config->vote));
  } else {
    debug_log("[-] POLICY: Failed to recognize global policy vote requirement; default to 1.00\n");
    *(config->vote) = 1.00;
  }

  fetch_command_policies(cfg, config);
  fetch_host_policies(cfg, config);

  // Keep this NULL for now so it's easy to check later; it gets populated in connect() hook
  config->command_name = NULL;

  // Register the config struct so that it becomes globally available
  global_config = config;

  // Cleanup
  config_destroy(&cfg);

  return 0;
}
