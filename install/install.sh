#!/bin/bash

PREFIX=/usr/local
INSTDIR=$PREFIX/etc/converge
DBDIR=/var/lib/converge
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

mkdir -p $DBDIR
mkdir -p $HOME/.converge
mkdir -p $HOME/bin

mkdir -p $INSTDIR/notaries-available
mkdir -p $INSTDIR/notaries-enabled

cp $SCRIPTDIR/converge.config $INSTDIR
cp $SCRIPTDIR/*.notary $INSTDIR/notaries-available

for n in $(ls $INSTDIR/notaries-available)
do
	ln -f -s $INSTDIR/notaries-available/$n $INSTDIR/notaries-enabled/$n
done


sqlite3 $DBDIR/converge.db < $SCRIPTDIR/converge.sql
sqlite3 $DBDIR/cache.db < $SCRIPTDIR/cache.sql

sqlite3 $HOME/.converge/converge.db < $SCRIPTDIR/converge.sql
sqlite3 $HOME/.converge/hook.db < $SCRIPTDIR/hook.sql
sqlite3 $HOME/.converge/cache.db < $SCRIPTDIR/cache.sql

chown -R $SUDO_USER:$SUDO_USER $HOME/.converge
chown $SUDO_USER:$SUDO_USER $HOME/bin/cconverge
