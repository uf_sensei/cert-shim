/*
   2013 -- Ryan Leonard <ryan.leonard71@gmail.com>
   See cache.h for description.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "common.h"
#include "cache.h"

int check_cache(char *host, char *port, char *fingerprint, sqlite3 *db){
  char *q ="SELECT * "
        "FROM fingerprints "
        "WHERE location = ? AND fingerprint = ?";
  char location[MAX_STRLEN_LOCATION];
  sqlite3_stmt *stmt;
  int ret, retval = 0;

  // compile our location string
  ret = snprintf(location, MAX_STRLEN_LOCATION, "%s:%s", host, port);
  location[MAX_STRLEN_LOCATION-1] = '\0';

  if (ret < 0){
    debug_log("[-] check_cache snprintf location.\n");
    exit(201);
  }

  // prepare sqlite statement
  ret = sqlite3_prepare_v2(db, q, -1, &stmt, NULL);

  if (ret != SQLITE_OK) {
    debug_log("[-] check_cache prepare: %s\n", sqlite3_errmsg(db));
    exit(201);
  }

  // bind location
  ret = sqlite3_bind_text(stmt, 1, location, -1, SQLITE_STATIC);

  if (ret != SQLITE_OK) {
    debug_log("[-] check_cache bind location: %s\n", sqlite3_errmsg(db));
    exit(201);
  }

  // bind fingerprint
  ret = sqlite3_bind_text(stmt, 2, fingerprint, -1, SQLITE_STATIC);

  if (ret != SQLITE_OK) {
    debug_log("[-] check_cache bind fingerprint: %s\n", sqlite3_errmsg(db));
    exit(201);
  }

  while (1) {
    ret = sqlite3_step(stmt);

    if (ret == SQLITE_ROW) {
      debug_log("[+] CACHE SUBSYSTEM: cache hit\n");
      retval = 0;

      break;
    } else if (ret == SQLITE_DONE) {
      debug_log("[+] CACHE SUBSYSTEM: cache miss\n");
      retval = 1;

      break;
    } else if (ret != SQLITE_BUSY) {
      debug_log("[-] check_cache step: %s\n", sqlite3_errmsg(db));

      exit(201);
    }
  }

  // finalize statement to avoid resource leaks
  ret = sqlite3_finalize(stmt);

  if (ret != SQLITE_OK) {
    debug_log("[-] check_cache finalize: %s\n", sqlite3_errmsg(db));
    exit(201);
  }

  return retval;
}



int update_cache(char *host, char *port, char *fingerprint, sqlite3 *db){
  char *q = "INSERT OR IGNORE INTO fingerprints (location, fingerprint) VALUES (?, ?)";
  char location[MAX_STRLEN_LOCATION];
  sqlite3_stmt *stmt;
  int ret, retval;

  // compile our location string
  ret = snprintf(location, MAX_STRLEN_LOCATION, "%s:%s", host, port);
  location[MAX_STRLEN_LOCATION-1] = '\0';

  if (ret < 0){
    debug_log("[-] update_cache snprintf location.\n");
    exit(202);
  }

  // prepare sqlite statement
  ret = sqlite3_prepare_v2(db, q, -1, &stmt, NULL);

  if (ret != SQLITE_OK) {
    debug_log("[-] update_cache prepare: %s\n", sqlite3_errmsg(db));
    exit(202);
  }

  // bind location
  ret = sqlite3_bind_text(stmt, 1, location, -1, SQLITE_STATIC);

  if (ret != SQLITE_OK) {
    debug_log("[-] update_cache bind location: %s\n", sqlite3_errmsg(db));
    exit(202);
  }

  // bind fingerprint
  ret = sqlite3_bind_text(stmt, 2, fingerprint, -1, SQLITE_STATIC);

  if (ret != SQLITE_OK) {
    debug_log("[-] update_cache bind fingerprint: %s\n", sqlite3_errmsg(db));
    exit(202);
  }

  // Execute the statement
  ret = sqlite3_step(stmt);
  if (ret == SQLITE_READONLY){
    retval = -1;
  } else if (ret == SQLITE_DONE) {
    retval = 0;
  } else {
    debug_log("[-] update_cache step: %s\n", sqlite3_errmsg(db));
    exit(202);
  }

    // finalize statement to avoid resource leaks
  ret = sqlite3_finalize(stmt);

  if (ret != SQLITE_OK && ret != SQLITE_READONLY) {
    debug_log("[-] update_cache finalize: %s\n", sqlite3_errmsg(db));
    exit(202);
  }

    return retval;
}

int check_cache_hp(char *host, char *port, sqlite3 *db){
  char *q ="SELECT * "
        "FROM fingerprints "
        "WHERE location = ?;";
  char location[MAX_STRLEN_LOCATION];
  sqlite3_stmt *stmt;
  int ret, retval = 0;

  // compile our location string
  ret = snprintf(location, MAX_STRLEN_LOCATION, "%s:%s", host, port);
  location[MAX_STRLEN_LOCATION-1] = '\0';

  if (ret < 0){
    debug_log("[-] check_cache snprintf location.\n");
    exit(201);
  }

  // prepare sqlite statement
  ret = sqlite3_prepare_v2(db, q, -1, &stmt, NULL);

  if (ret != SQLITE_OK) {
    debug_log("[-] check_cache prepare: %s\n", sqlite3_errmsg(db));
    exit(201);
  }

  // bind location
  ret = sqlite3_bind_text(stmt, 1, location, -1, SQLITE_STATIC);

  if (ret != SQLITE_OK) {
    debug_log("[-] check_cache bind location: %s\n", sqlite3_errmsg(db));
    exit(201);
  }

  // Execute the statement
  while (1){
    ret = sqlite3_step(stmt);

    if (ret == SQLITE_ROW) {
      //retval = 1;
      retval = 0;
      break;
    } else if (ret == SQLITE_DONE) {
      //retval = 0;
      retval = 1;
      break;
    } else if (ret != SQLITE_BUSY) {
      debug_log("[-] check_cache step: %s\n", sqlite3_errmsg(db));
      exit(201);
    }
  }

  // finalize statement to avoid resource leaks
  ret = sqlite3_finalize(stmt);

  if (ret != SQLITE_OK) {
    debug_log("[-] check_cache finalize: %s\n", sqlite3_errmsg(db));
    exit(201);
  }

  return retval;
}
