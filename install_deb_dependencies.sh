apt-get install -y build-essential sqlite3 libsqlite3-dev libssl-dev libgnutls-dev libjson0-dev libcurl4-gnutls-dev curl libconfig8-dev libpolarssl-dev libnss3-dev libnspr4-dev

# The following are for the Swede library that we need to perform DANE lookups
apt-get install python-unbound python-m2crypto python-ipaddr 
