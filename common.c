#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

// Set to NULL for printing to stdout
// /dev/null to discard
#define LOGFILE NULL
#define DEBUG 1

FILE *logfd = NULL;

void debug_log(const char *fmt, ...) {
#if DEBUG
  va_list ap;
  char *dbg;

  dbg = getenv("CERTSHIM_DEBUG");

  if (!dbg || dbg[0] != '1') {
    return;
  }

  if (LOGFILE != NULL) {
    logfd = fopen(LOGFILE, "a+");
  } else {
    logfd = stderr;
  }

  va_start(ap, fmt);
  vfprintf(logfd, fmt, ap);
  va_end(ap);
#endif
}

void *xmalloc(unsigned int bytes) {
  void *ptr = NULL;

  ptr = calloc(bytes, 1);

  if (ptr) {
    return ptr;
  } else {
    exit(255);
  }
} 
