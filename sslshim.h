#ifndef _SSLSHIMH
#define _SSLSHIMH

// XXX Might could just get an entire copy of the GnuTLS source tree?
#include "gnutls/gnutls_int.h"

typedef struct rsa_info_st
{
  gnutls_datum_t modulus;
  gnutls_datum_t exponent;
} rsa_info_st;

typedef struct
{
  int secret_bits;

  gnutls_datum_t prime;
  gnutls_datum_t generator;
  gnutls_datum_t public_key;
} dh_info_st;

typedef struct cert_auth_info_st
{
  /* 
   * These (dh/rsa) are just copies from the credentials_t structure.
   * They must be freed.
   */
  dh_info_st dh;
  rsa_info_st rsa_export;

  gnutls_datum_t *raw_certificate_list; // Holds the raw cert of the peer
  unsigned int ncerts;                  // Holds the size of the above list

  gnutls_certificate_type_t cert_type;
#ifdef ENABLE_OPENPGP
  uint8_t subkey_id[GNUTLS_OPENPGP_KEYID_SIZE];
#endif
} *cert_auth_info_t;

void _sha_to_ascii(const unsigned char *in, char *out, unsigned int size);

#endif
