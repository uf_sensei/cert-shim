#define _GNU_SOURCE

#include <time.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <openssl/ssl.h>

#include <unistd.h> // getpid/getpgrp

#include <sys/wait.h> // waitpid

#include <sqlite3.h> // sqlite shit

#include "config.h"
#include "converge.h"
#include "common.h"
#include "cache.h"

#define SHIM_NAME       "converge.so"
#define HOOK_DB         "/.converge/hook.db"
#define CACHE_DB        "/.converge/cache.db"
#define SYS_CACHE_DB    "/var/lib/converge/cache.db"

#define PRINT_INIT 0

extern char **environ;
extern FILE *logfd;

// Global db pointers
sqlite3 *db;
sqlite3 *cache_db;
sqlite3 *sys_cache_db;

void converge_disable_sqlite_journaling(sqlite3 *database) {
  // sqlite> pragma synchronous=off;
  char *q = "pragma synchronous=off;";
  sqlite3_stmt *stmt;
  int ret;

  ret = sqlite3_prepare_v2(database, q, -1, &stmt, NULL);

  if (ret != SQLITE_OK) {
    debug_log("[-] converge_disable_sqlite_journaling prepare: %s\n", sqlite3_errmsg(database));
    exit(245);
  }

  ret = sqlite3_step(stmt);

  if (ret != SQLITE_DONE) {
    debug_log("[-] converge_disable_sqlite_journaling step: %s\n", sqlite3_errmsg(database));
    exit(245);
  }

  ret = sqlite3_finalize(stmt);

  if (ret != SQLITE_OK) {
    debug_log("[-] converge_disable_sqlite_journaling finalize: %s\n", sqlite3_errmsg(database));
    exit(245);
  }
}

void converge_init() {
  int ret;
  pid_t pid, grp;
  char *home, *dbloc;
  struct certshim_config *config = malloc(sizeof(struct certshim_config));

  converge_parse_config(config);

  pid = getpid();
  grp = getpgrp();

#if PRINT_INIT
  debug_log("\n");
  debug_log("[+] Hooked init <%d, %d>\n", pid, grp);
  debug_log("\tclearing db\n");
#endif

  home = getenv("HOME");
  
  // Setup hook db
  dbloc = xmalloc(strlen(home) + strlen(HOOK_DB) + 1);
  sprintf(dbloc, "%s%s", home, HOOK_DB);
  ret = sqlite3_open_v2(dbloc, &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_FULLMUTEX, NULL);

  if (ret != SQLITE_OK) {
    debug_log("[-] Could not open db (%s): %s\n", dbloc, sqlite3_errmsg(db));
    exit(241);
  }

  free(dbloc);

  converge_disable_sqlite_journaling(db);
  converge_db_clear(pid, grp);

  // Setup caching DB
  dbloc = xmalloc(strlen(home) + strlen(CACHE_DB) + 1);
  sprintf(dbloc, "%s%s", home, CACHE_DB);
  ret = sqlite3_open_v2(dbloc, &cache_db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_FULLMUTEX, NULL);
  free(dbloc);

  if (ret != SQLITE_OK) {
    debug_log("[-] Could not open cache db: %s\n", sqlite3_errmsg(cache_db));
    exit(241);
  }
  
  converge_disable_sqlite_journaling(cache_db);
  ret = sqlite3_open_v2(SYS_CACHE_DB, &sys_cache_db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_NOMUTEX, NULL);

  if (ret != SQLITE_OK) {
    debug_log("[-] Could not open system cache db: %s\n", sqlite3_errmsg(sys_cache_db));
    exit(241);
  }

  converge_disable_sqlite_journaling(sys_cache_db);
}

void converge_fini() {
  // See todo below
  //pid_t pid, grp;
  int ret;

  //pid = getpid();
  //grp = getpgrp();

#if PRINT_INIT  
  debug_log("\n");
  debug_log("[+] Hooked fini <%d, %d>\n", pid, grp);
  debug_log("\tclearing db\n");
#endif

  // XXX TODO 
  // We need to change this. It needs to be smarter in how it clears the db
  // when an app exits, its children pgrp gets set to 1, so if we clear all
  // child apps will not be able to access the parents fd
  //converge_db_clear(pid, grp);

  // close hook db
  ret = sqlite3_close(db);

  while (ret == SQLITE_BUSY) {
    ret = sqlite3_close(db);
  }

  if (ret != SQLITE_OK) {
    debug_log("[-] Could not close db: %s\n", sqlite3_errmsg(db));
    exit(242);
  }

  // close cache db
  ret = sqlite3_close(cache_db);

  while (ret == SQLITE_BUSY) {
    ret = sqlite3_close(cache_db);
  }

  if (ret != SQLITE_OK) {
    debug_log("[-] Could not close cache db: %s\n", sqlite3_errmsg(cache_db));
    exit(242);
  }

  // close system wide cache db
  ret = sqlite3_close(sys_cache_db);

  while (ret == SQLITE_BUSY) {
    ret = sqlite3_close(sys_cache_db);
  }

  if (ret != SQLITE_OK) {
    debug_log("[-] Could not close system cache db: %s\n", sqlite3_errmsg(cache_db));
    exit(242);
  }

  // Close the global config file
  destroy_config(global_config);

  // XXX TODO we need to close the log file, and deal with the whole
  // many processes writing to the log file. Pain in the ass, but not
  // critical. For "production" use, it will just write to /dev/null
  // anyways
}

int notarize(char *host, char *port, char *fingerprint) 
{
  char **env;
  int ret = 1, forkret;
  char *args[5];
  pid_t pid;

  debug_log("[+] Calling cconverge with exec(): %s %s %s %s %s\n", 
      "cconverge", "cconverge", host, port, fingerprint);

  env = copy_env();
  remove_shim(env);

  pid = vfork();

  if (pid) {
    // parent
    waitpid(pid, &ret, 0);
  } else {
    // child
    args[0] = "cconverge";
    args[1] = host;
    args[2] = port;
    args[3] = fingerprint;
    args[4] = NULL;

    forkret = execvpe(args[0], args, env);

    // If we reach this code, it means that cconverge failed to execute.
    perror("converge");
    debug_log("[-] execvpe returned: %d\n", forkret);
    exit(243);
  }

  free_env(env);
  
  if (WIFEXITED(ret)) {
    return WEXITSTATUS(ret);
  } else {
    // Return -1 for error
    return -1;
  }
}

int converge(char *host, char *port, char *fingerprint) {
  int err, ret;

  // Check the user and system caches
  ret = check_cache(host, port, fingerprint, cache_db);
  if (ret) {
      ret = check_cache(host, port, fingerprint, sys_cache_db);
  }

  if (!ret) {
    debug_log("[+] CONVERGENCE: Cache hit for (%s, %s, %s) return OK\n", host, port, fingerprint);

    return 0;
  }

  // If we did not have any cache hits, notarize
  ret = notarize(host, port, fingerprint);

  // If notarize() checks out, update the cache and return success
  if (!ret) {
    err = update_cache(host, port, fingerprint, cache_db);

    if (err == -1 ) {
      debug_log("[-] CONVERGENCE: Could not write to user cache.\n");
      exit(244);
    }

    return 0;
  }

  // If notarize() doesn't check out, return error
  return 1;
}

void converge_getaddrinfo(const char *host, const char *ip) {
  pid_t pid, grp;
  // sqlite> create table getaddrinfo (pid int, grp int, host text, ip text);
  char *q = "INSERT INTO getaddrinfo VALUES (?, ?, ?, ?);";
  sqlite3_stmt *stmt;
  int ret;

  pid = getpid();
  grp = getpgrp();

  ret = sqlite3_prepare_v2(db, q, -1, &stmt, NULL);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_getaddrinfo prepare: %s\n", sqlite3_errmsg(db));
    exit(245);
  }

  // Bind pid
  ret = sqlite3_bind_int(stmt, 1, pid);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_getaddrinfo bind pid: %s\n", sqlite3_errmsg(db));
    exit(245);
  }

  // Bind grp
  ret = sqlite3_bind_int(stmt, 2, grp);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_getaddrinfo bind grp: %s\n", sqlite3_errmsg(db));
    exit(245);
  }

  // Bind hostname
  ret = sqlite3_bind_text(stmt, 3, host, -1, SQLITE_STATIC);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_getaddrinfo bind host: %s\n", sqlite3_errmsg(db));
    exit(245);
  }

  // Bind ip
  ret = sqlite3_bind_text(stmt, 4, ip, -1, SQLITE_STATIC);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_getaddrinfo bind: %s\n", sqlite3_errmsg(db));
    exit(245);
  }

  ret = sqlite3_step(stmt);
  if (ret != SQLITE_DONE) {
    debug_log("[-] converge_getaddrinfo step: %s\n", sqlite3_errmsg(db));
    exit(245);
  }

  ret = sqlite3_finalize(stmt);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_getaddrinfo finalize: %s\n", sqlite3_errmsg(db));
    exit(245);
  }
}

void converge_connect(char *ip, unsigned int port, int fd) {
  pid_t pid, grp;
  // sqlite> create table connect (pid int, grp int, ip text, port int, fd int, 
  //   constraint connect_unique unique(pid, grp, fd) on conflict replace);
  char *q = "INSERT INTO connect VALUES (?, ?, ?, ?, ?);";
  sqlite3_stmt *stmt;
  int ret;

  pid = getpid();
  grp = getpgrp();
  
  ret = sqlite3_prepare_v2(db, q, -1, &stmt, NULL);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_connect prepare: %s\n", sqlite3_errmsg(db));
    exit(246);
  }

  // Bind pid
  ret = sqlite3_bind_int(stmt, 1, pid);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_connect bind pid: %s\n", sqlite3_errmsg(db));
    exit(246);
  }

  // Bind grp
  ret = sqlite3_bind_int(stmt, 2, grp);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_connect bind grp: %s\n", sqlite3_errmsg(db));
    exit(246);
  }

  // Bind ip 
  ret = sqlite3_bind_text(stmt, 3, ip, -1, SQLITE_STATIC);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_connect bind ip: %s\n", sqlite3_errmsg(db));
    exit(246);
  }

  // Bind port
  ret = sqlite3_bind_int(stmt, 4, port);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_connect bind port: %s\n", sqlite3_errmsg(db));
    exit(246);
  }

  // Bind fd
  ret = sqlite3_bind_int(stmt, 5, fd);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_connect bind fd: %s\n", sqlite3_errmsg(db));
    exit(246);
  }

  do {
    ret = sqlite3_step(stmt);

    if (ret != SQLITE_DONE) {
      debug_log("[-] converge_connect step: %s\n", sqlite3_errmsg(db));
      debug_log("[*] Reattempting converge_connect_step...\n");
    }
  } while (ret != SQLITE_DONE);

  ret = sqlite3_finalize(stmt);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_connect finalize: %s\n", sqlite3_errmsg(db));
    exit(246);
  }
}

void converge_get_host_openssl(const SSL *ssl, char **host, char **port) {
  if (!ssl) {
    debug_log("[-] Bad struct SSL* to SSL_get_verify_result()\n");
    exit(247);
  }

  // This is kinda weird, basically we want to run these functions until one
  // returns true, then stop.
  if (converge_bio_host(ssl, host, port)) {
    ;
  } else if (converge_fd_host_ssl(ssl, host, port)) {
    ;
  } else if (!*host || !*port) {
    debug_log("[-] Could not get host:port\n");
    exit(247);
  }
}

void converge_get_host_gnutls(const gnutls_session_t session, char **host, char **port) {
  if (!session) {
    debug_log("[-] Bad struct gnutls_session_t* to _gnutls_x509_cert_verify_peers()\n");
    exit(248);
  }

  if (converge_fd_host_gnutls(session, host, port)) {
    ;
  } else if (!*host || !*port) {
    debug_log("[-] Could not get host:port\n");
    exit(248);
  }
}

//begin:added by lzhzh <zoeliu@ufl.edu>
void converge_get_host_polarssl(const ssl_context *ssl, char **host, char **port) {
  if (!ssl) {
    debug_log("[-] Bad struct ssl_session* to _gnutls_x509_cert_verify_peers()\n");
    exit(248);
  }

  if (converge_fd_host_polarssl(ssl, host, port)) {
    ;
  } else if (!*host || !*port) {
    debug_log("[-] Could not get host:port\n");
    exit(248);
  }
}
//end : added by lzhzh <zoeliu@ufl.edu>


//NSS start - Added code to get the host and port information from the NSPR file descriptor
void converge_get_host_nss(PRFileDesc *fileDesc, char **host, char **port) {
  if(!fileDesc) {
    debug_log("[-] Bad struct PRFileDesc* to SSL_ResetHandshake()\n");
    exit(249);
  }

  if(converge_fd_host_nss(fileDesc, host, port)) {
    ;
  } else if(!*host || !*port) {
    debug_log("[-] Could not get host:port\n");
    exit(249);
  }
}
//NSS end

int converge_bio_host(const SSL *ssl, char **host, char **port) {
  // First we try to get the hostname/port out of the SSL data struct
  // This will work if they used functions like BIO_set_conn_hostname()
  BIO_CONNECT *data = ssl->rbio->ptr;
  unsigned int len;

  if (!ssl->rbio) {
    return 0;
  }

  // XXX We might not want to use strlen here, possible null char in str
  if (data && strlen(data->param_hostname) && strlen(data->param_port)) {
    len = strlen(data->param_hostname) + 1;
    *host = xmalloc(len);
    strncpy(*host, data->param_hostname, len);
    *host[len - 1] = '\0';

    len = strlen(data->param_port) + 1;
    *port = xmalloc(len);
    strncpy(*port, data->param_port, len);
    *port[len - 1] = '\0';

    debug_log("[+] converge_bio_host()\n");
    debug_log("\thost: %s:%s\n", *host, *port);

    return 1;
  }

  return 0;
}

//begin: added by lzhzh <zoeliu@ufl.edu>
int converge_fd_host_polarssl(const ssl_context *ssl, char **host, char **port){
  int fd = -1;
  pid_t pid,grp;

  pid = getpid();
  grp = getpgrp();

  debug_log("[+] converge_fd_host_polarssl() <%d, %d>\n",pid,grp);
  
  // THIS IS THE LINE YOU NEED TO WRITE
  // Extract the file descriptor for the socket from the
  // PolarSSL context. It has to be in there somewhere.

  //fd = (int) XXX->something->something->something;
  fd = *((int *) ssl->p_recv);
  return converge_get_fd_host(pid,grp,fd,host,port);
  
}
//end: added by lzhzh <zoeliu@ufl.edu>

int converge_fd_host_ssl(const SSL *ssl, char **host, char **port) {
  int fd = -1;
  pid_t pid, grp;

  pid = getpid();
  grp = getpgrp();

  if (!ssl->rbio) {
    return 0;
  }

  debug_log("[+] converge_fd_host_ssl() <%d, %d>\n", pid, grp);

  // XXX Note, reading the code indicates that ssl->rbio and ssl->wbio should
  // be the same, but they're not. Presumably they have different fd's in
  // their respective num fields. Im fairly certain this is correct, but we
  // should investigate and try to figure out why rbio != wbio
  fd = ssl->rbio->num;

  return converge_get_fd_host(pid, grp, fd, host, port);
}

int converge_fd_host_gnutls(const gnutls_session_t session, char **host, char **port) {
  int fd = -1;
  pid_t pid, grp;

  pid = getpid();
  grp = getpgrp();

  debug_log("[+] converge_fd_host_gnutls() <%d, %d>\n", pid, grp);

  fd = (int)session->internals.transport_send_ptr;

  return converge_get_fd_host(pid, grp, fd, host, port);
}

//NSS start - Added code to get the host and port information from the NSPR file descriptor
int converge_fd_host_nss(PRFileDesc *fileDesc, char **host, char **port) {
  int fd = -1;
  pid_t pid, grp;

  pid = getpid();
  grp = getpgrp();

  debug_log("[+] converge_fd_host_nss() <%d, %d>\n", pid, grp);

  /* NSS converts the POSIX file descriptor into NSPR file descriptor so we need
  to convert it back to native file descriptor for extracting the host and port information*/
  fd = (int) PR_FileDesc2NativeHandle(fileDesc);

  return converge_get_fd_host(pid, grp, fd, host, port);
}
//NSS end

int converge_get_fd_host(pid_t pid, pid_t grp, int fd, char **host, char **port) {
  if (pid == grp || grp != 1) {
    converge_fd_host_parent(pid, grp, fd, host, port);
  } else if (grp == 1) {
    // child where parent has been killed
  }

  if (*host && strlen(*host) && *port && strlen(*port)) {
    debug_log("[+] converge_get_fd_host()\n");
    debug_log("\thost: %s:%s\n", *host, *port);
    return 1;
  }

  return 0;
}

void converge_fd_host_parent(pid_t pid, pid_t grp, int fd, char **host, char **port) {
  sqlite3_stmt *stmt;
  int ret, columns, col;
  unsigned int len;
  const char *val;
  char *query = "SELECT DISTINCT connect.port, getaddrinfo.host "
      "FROM connect INNER JOIN getaddrinfo ON connect.pid = getaddrinfo.pid "
      "AND connect.ip = getaddrinfo.ip "
      "WHERE connect.fd = ? AND (connect.pid = ? OR connect.grp = ?);";

  debug_log("[+] converge_fd_host_parent() query:\n");
  debug_log("\t SELECT DISTINCT connect.port, getaddrinfo.host "
    "FROM connect INNER JOIN getaddrinfo ON connect.pid = getaddrinfo.pid "
    "AND connect.ip = getaddrinfo.ip "
    "WHERE connect.fd = %d AND (connect.pid = %d OR connect.grp = %d);\n", fd, pid, grp);

  ret = sqlite3_prepare_v2(db, query, -1, &stmt, NULL);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_fd_host_parent prepare: %s\n", sqlite3_errmsg(db));
    exit(248);
  }

  // Bind fd
  ret = sqlite3_bind_int(stmt, 1, fd);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_fd_host_parent bind fd: %s\n", sqlite3_errmsg(db));
    exit(248);
  }

  // Bind pid
  ret = sqlite3_bind_int(stmt, 2, pid);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_fd_host_parent bind pid: %s\n", sqlite3_errmsg(db));
    exit(248);
  }

  ret = sqlite3_bind_int(stmt, 3, grp);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_fd_host_parent bind grp: %s\n", sqlite3_errmsg(db));
    exit(248);
  }

  columns = sqlite3_column_count(stmt);
  while(1) {
    ret = sqlite3_step(stmt);

    if (ret == SQLITE_ROW) {
      for (col = 0; col < columns; col++) {
        val = (const char*)sqlite3_column_text(stmt,col);
        len = strlen(val) + 1;

        if (!strcmp(sqlite3_column_name(stmt, col), "port")) {
          *port = xmalloc(len);
          strncpy(*port, val, len);
          debug_log("[+] converge_fd_host_parent port name: %s\n", *port);
        } else if (!strcmp(sqlite3_column_name(stmt, col), "host")) {
          *host = xmalloc(len);
          strncpy(*host, val, len);
          debug_log("[+] converge_fd_host_parent host name: %s\n", *host);
        } else {
          debug_log("[-] converge_fd_host_parent column name: %s\n", 
              sqlite3_column_name(stmt, col));
          exit(248);
        }
      }   
      break;
    } else if (ret == SQLITE_DONE) {
      debug_log("[-] converge_fd_host_parent SQLITE_DONE\n");
      break;
    } else if (ret != SQLITE_BUSY) {
      debug_log("[-] converge_fd_host_parent step: %s\n", sqlite3_errmsg(db));
      exit(248);
    }

  }

  ret = sqlite3_finalize(stmt);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_fd_host_parent finalize: %s\n", sqlite3_errmsg(db));
    exit(248);
  }
}

void converge_db_clear(pid_t pid, pid_t grp) {
  sqlite3_stmt *stmt;
  // We delete by grp because fork'd processes wont run converge_init, so 
  // possibility of old rows being hit, which is very bad
  char *clear_connect = "DELETE FROM connect WHERE grp = ?";
  char *clear_getaddrinfo = "DELETE FROM getaddrinfo WHERE grp = ?";
  int ret;

  do {
    ret = sqlite3_prepare_v2(db, clear_connect, -1, &stmt, NULL);

    if (ret != SQLITE_OK) {
      debug_log("[-] converge_db_clear prepare: %s\n", sqlite3_errmsg(db));
      debug_log("[*] Reattempting to prepare SQL statement...\n");
    }
  } while (ret != SQLITE_OK);
  // Not sure if bind can fail like other DB calls...
  ret = sqlite3_bind_int(stmt, 1, pid);

  if (ret != SQLITE_OK) {
    debug_log("[-] converge_db_clear bind pid: %s\n", sqlite3_errmsg(db));
    exit(249);
  }

  do {
    ret = sqlite3_step(stmt);

    if (ret != SQLITE_DONE) {
      debug_log("[-] converge_db_clear step: %s\n", sqlite3_errmsg(db));
      debug_log("[*] Attempting to reset and re-execute...\n");

      sqlite3_reset(stmt);
    }
  } while (ret != SQLITE_DONE);

  do {
    ret = sqlite3_step(stmt);

    if (ret != SQLITE_DONE) {
      debug_log("[-] converge_db_clear step: %s\n", sqlite3_errmsg(db));
      debug_log("[*] Attempting to reset and re-execute...\n");

      sqlite3_reset(stmt);
    }
  } while (ret != SQLITE_DONE);

  ret = sqlite3_finalize(stmt);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_db_clear finalize: %s\n", sqlite3_errmsg(db));
    exit(249);
  }

  ret = sqlite3_prepare_v2(db, clear_getaddrinfo, -1, &stmt, NULL);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_db_clear prepare: %s\n", sqlite3_errmsg(db));
    exit(249);
  }

  ret = sqlite3_bind_int(stmt, 1, pid);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_db_clear bind pid: %s\n", sqlite3_errmsg(db));
    exit(249);
  }

  ret = sqlite3_step(stmt);
  if (ret != SQLITE_DONE) {
    debug_log("[-] converge_db_clear step: %s\n", sqlite3_errmsg(db));
    exit(249);
  }

  ret = sqlite3_finalize(stmt);
  if (ret != SQLITE_OK) {
    debug_log("[-] converge_db_clear finalize: %s\n", sqlite3_errmsg(db));
    exit(249);
  }
}

char **copy_env() {
  char **env;
  unsigned int count = 0, i;

  for (env = environ; *env; env++) {
    count++;
  }

  // One more for final NULL*
  count++;

  env = xmalloc(count * sizeof(char *));

  for (i = 0; environ[i]; i++) {
    count = strlen(environ[i]) + 1;
    env[i] = xmalloc(count);
    strncpy(env[i], environ[i], count);
  }

  env[i] = NULL;

  return env;
}

void free_env(char **env) {
  char **p;

  for (p = env; *p ; p++) {
    free(*p);
  }

  free(env);
}

void remove_shim(char **env) {
  char **e, *ptr, *buf;
  unsigned int shim_len, env_len, diff;

  for (e = env; *e; e++) {
    ptr = strstr(*e, "LD_PRELOAD");
    if (!ptr) {
      continue;
    }

    // If LD_PRELOAD is at the start of the string it's the env element
    // we're looking for
    if (!(*e - ptr)) {
      break;
    }
  }

  ptr = strstr(*e, SHIM_NAME);

  // No shim in LD_PRELOAD, what are we doing here?
  // This is some inception-esque code if you think about it
  if (!ptr) {
    return;
  }

  env_len = strlen(*e);
  shim_len = strlen(SHIM_NAME);

  // If the character followingthe shim is a space, we want to remove it
  // as well so we don't end up with 'LD_PRELOAD= foo.so'
  if (*(ptr + shim_len) == ' ') {
    shim_len++;
  }

  // If we have '/foo/bar/shim.so', we need to remove '/foo/bar' too
  while (!(*(ptr - 1) == '=' || *(ptr - 1) == ' ')) {
    ptr--;
    shim_len++;
  }

  // Shitty pointer math
  diff = (*e+env_len - ptr - shim_len) + 1; //End of env - end of buf

  // We need another buf to avoid undef behavior with overlapping
  // buffers
  buf = xmalloc(diff);

  // Copy from the end of the shim until the end of the env entr
  strncpy(buf, ptr + shim_len, diff);
  // Copy it all back to the start of the shim
  strncpy(ptr, buf, diff);

  free(buf);
}
//*/

int dane_verify(char * port, char * host){
  char **env;
  int daneret = 0, forkret;
  char *args[6];
  pid_t newpid;

  debug_log("[+] Attempting DANE verification\n");

  env = copy_env();
  remove_shim(env);

  newpid = vfork();

  if (newpid) {
    // parent
    waitpid(newpid, &daneret, 0);
  } else {
    // child
    args[0] = "swede";
    args[1] = "verify";
    args[2] = "-p";
    args[3] = port;
    args[4] = host;
    args[5] = NULL;
    forkret = execvpe(args[0], args, env);

    perror("dane");
    debug_log("[-] execvpe returned: %d\n", forkret);
    exit(243);
  }

  free_env(env);

  if (WEXITSTATUS(daneret)) {
    debug_log("[-] Dane Verification has failed! Returning: %d\n", daneret);
    return -1;
  } else {
    debug_log("[+] Dane Verification has succeeded!\n");
    return 0;
  }     
}

// Returns 0 on Success, -1 on failure
int cert_pin_check(char *host, char *port, char *fingerprint) {
  int hpret, fret, err;

  debug_log("[+] Checking Certificate Pin for (%s, %s, %s)\n", host, port, fingerprint);

  // See if the host/port has ever been visited.
  hpret = check_cache_hp(host, port, cache_db);
  if (hpret) {
    hpret = check_cache_hp(host, port, sys_cache_db);
  }

  // If it has been visited, see if the fingerprints match
  if (!hpret) {
    // TODO If cache hit, check to see if the fingerprints match
    // This currently does not check the fingerprints, just the cache.

    // Check our user cache, then check the system cache
    fret = check_cache(host, port, fingerprint, cache_db);
    if (fret) { // we had a cache miss from the user's cache
      fret = check_cache(host, port, fingerprint, sys_cache_db);
    }
  }

  // We have never visited to this host, SUCCESS
  if(hpret) {
    debug_log("[+] CertPin: First use for (%s, %s, %s) return OK\n", host, port, fingerprint);

    // Cache this shit
    err = update_cache(host, port, fingerprint, cache_db);

    if (err == -1 ) {
      debug_log("[-] Could not cache pinned certificate.\n");
      exit(244);
    }

    return 0;
  }
    
  // The certificate is the same as the last time we saw it, SUCCESS
  // TODO I'm not sure I see where fret is set if the cert matches the cache...
  if(!fret){
    debug_log("[+] Certicate pin match (%s, %s, %s) return OK\n", host, port, fingerprint);
    return 0;
  }

  // We went to the host and it didn't match our fingerprint, FAIL
  // TODO I'm not sure I see where fret is set if the cert doesn't match the cache...
  if(fret){
    debug_log("[-] Certificate Pin does not match (%s, %s, %s) return FAIL\n", host, port, fingerprint);
    return -1;
  }

  printf("We shouldn't have gotten here");
  return -1;
}
