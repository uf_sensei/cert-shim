#ifndef _CONVERGEH
#define _CONVERGEH

#include <netinet/in.h>
#include <gnutls/gnutls.h>
#include <polarssl/ssl.h>

#include "gnutls/gnutls_int.h"
#include <prio.h>
//AMB: LZHZH commented out: probably safe to remove?
//#include "/home/lzz/cert-shim/polarssl/include/polarssl/ssl.h"

// XXX TODO This should be moved into its own folder, similar to GnuTLS

// From OpenSSL - crypto/bio/bss_conn.c
typedef struct bio_connect_st
{   
  int state;

  char *param_hostname;
  char *param_port;
  int nbio;

  unsigned char ip[4];
  unsigned short port;

  struct sockaddr_in them;

  /* int socket; this will be kept in bio->num so that it is
   * compatible with the bss_sock bio */ 

  /* called when the connection is initially made
   * callback(BIO,state,ret);  The callback should return
   * 'ret'.  state is for compatibility with the ssl info_callback */
  int (*info_callback)(const BIO *bio,int state,int ret);
} BIO_CONNECT;

void __attribute__ ((constructor)) converge_init(void);
void __attribute__ ((destructor)) converge_fini(void);

int converge(char *host, char *port, char *fingerprint);
void converge_getaddrinfo(const char *host, const char *ip);
void converge_connect(char *ip, unsigned int port, int fd);
void converge_get_host_openssl(const SSL *ssl, char **host, char **port);
void converge_get_host_gnutls(const gnutls_session_t session, char **host, char **port);
void converge_get_host_polarssl(const ssl_context *session, char **host, char **port);
//NSS start
void converge_get_host_nss(PRFileDesc *fileDesc, char **host, char **port);
//NSS end
int converge_bio_host(const SSL *ssl, char **host, char **port);
int converge_fd_host_ssl(const SSL *ssl, char **host, char **port);
int converge_fd_host_gnutls(const gnutls_session_t session, char **host, char **port);
int converge_fd_host_polarssl(const ssl_context *session, char **host, char **port);
//NSS start
int converge_fd_host_nss(PRFileDesc *fileDesc, char **host, char **port);
//NSS end
int converge_get_fd_host(pid_t pid, pid_t grp, int fd, char **host, char **port);
void converge_fd_host_parent(pid_t pid, pid_t grp, int fd, char **host, char **port);
void converge_db_clear(pid_t pid, pid_t grp);
int dane_verify(char * port, char * host);
int cert_pin_check(char *host, char *port, char *fingerprint);
char **copy_env(void);
void free_env(char **env);
void remove_shim(char **env);

#endif
