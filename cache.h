#ifndef _CACHEH
#define _CACHEH

/*
 2013 -- Ryan Leonard <ryan.leonard71@gmail.com>
 cache.c offers a pair of functions for interface with a sqlite DB
 holding a unique server tag mapped to a "state." The state being XXX.
*/
#include <sqlite3.h> // sqlite include

#define MAX_QUERRY_LEN 512
#define MAX_STRLEN_LOCATION 64


/*
  2013 -- Ryan Leonard <ryan.leonard71@gmail.com>
  Description: Will check cache_db for a particular (host,port,fingerprint) tuple, 
      the return value indicating whether the tuple was in the cache or not.
  Return: 1 implies cache hit, 0 implies cache miss. 
*/
int check_cache(char *host, char *port, char *fingerprint, sqlite3 *db);


// Same as check cache, except doesnt use a fingerprint as a lookup parameter.`
int check_cache_hp(char *host, char *port, sqlite3 *db);


/*
  2013 -- Ryan Leonard <ryan.leonard71@gmail.com>
  Description: Will insert a state into cache_db corresponding to the entry
      identified by (host, port).
  Return: 0 upon success. -1 indicates unable to write to cache_db.
*/
int update_cache(char *host, char *port, char *fingerprint, sqlite3 *db);

#endif
