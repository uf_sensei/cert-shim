#ifndef _CONVERGE_CONFIG
#define _CONVERGE_CONFIG

// Includes

// Defines

// Structs
struct policy {
  char *command, *host;
  double *vote;
  struct verification_methods {
    int *cert_pinning;
    int *convergence;
    int *dane;
    int *cert_authority;
  } *methods;

  struct policy *previous, *next, *head;
};

struct certshim_config {
  // Verification methods; 0 = false (do not use), 1 = true (use)
  int cert_pinning, cert_pinning_status;
  int convergence, convergence_status;
  int dane, dane_status;
  int cert_authority, cert_authority_status;
  double *vote;

  char *command_name;

  int n_command_policies, n_host_policies;
  struct policy *command_policies, *host_policies;
};

// Variables to export
struct certshim_config *global_config;

// Prototypes for functions intended for external use
void destroy_config (struct certshim_config *);
struct policy *get_command_policy (const char *, struct certshim_config *);
struct policy *get_host_policy (const char *, struct certshim_config *);
int converge_parse_config (struct certshim_config *);
#endif
