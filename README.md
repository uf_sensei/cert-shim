# Installation
## Resolve dependencies
    $ sudo ./install_deb_dependencies.sh

## Deploy basic convergence resources
    $ sudo ./install/install.sh

**Note** The install script installs to

* /usr/local/etc/converge
* /var/lib/converge
* ~/.converge

## Build
    $ make

## Set environment variables
    $ export PATH=$PATH:$HOME/bin/
    $ export LD_PRELOAD=/path/to/converge.so
    $ export CERTSHIM_CONFIG=/path/to/certshim.cfg

**Note** For persistence across reboots/logouts, append these lines to your ~/.bashrc file.

## (Optional) Enable/disable debug logging
    $ export CERTSHIM_DEBUG=1

**Note** By default, debug logging is printed to STDOUT. To direct output to a logfile, alter #define LOGFILE in common.c to point to the desired logfile.

## Developer Notes
Make sure to run
    $ make clean && make
any time you want to rebuild CertShim.

# About
Cert-Shim is an LD_PRELOAD shim that seamlessly ports existing dynamically  linked code to Convergence and other CA alternatives. It works by hooking calls to SSL_get_verify_result(3), gnutls_certificate_verify_peers3(3) and others to alternative verification methods on the presented certificate using information found in memory. The hooked functions return and set the appropriate error code the calling program should expect, allowing for  (hopefully) painless integration.

To perform validation via some of these methods, we require the hostname, the port, and the sha1 hash of the x509 certificate. Unfortunately, neither the SSL ctx struct nor the Gnutls session struct provide us with this information in deterministic places. To get around this, as both GnuTLS and OpenSSL bind to file descriptors, we use a local SQLite database and hook the return values from getaddrinfo(3), gethostbyname(3), and connect(3) to create a mapping from file descriptor to hostname and port. The x509 certificate exists easily in both data structures.

We make liberal use of caching to reduce overhead, and early test suggest that this process ends up costing us very little. We do not currently support notary proxying.
 
Example: http://pastie.org/7705718

Contacts: 
  Joe Pletcher <joepletcher@gmail.com>
  Tyler Nichols <tyler.n.89@gmail.com>
  Adam Bates <bates151@gmail.com>

## Known Bugs
* If the program has multiple threads which connect to different vhosts on the 
same IP address, the library will be unable to properly associate the correct
thread with the correct vhost
* If a parent process creates a file descriptor, bind an SSL context to it, 
SSL connection but doesn't verify it, and passes it to a child process via 
fork, then the parent process is killed, the client will be unable to do the 
necessary reverse lookup to verify.

## Version information
Beause we're accessing internal data structures, the library versions are 
kinda important. This code right now works with GnuTLS 2.12.14 which is stock 
with Ubunutu 12.10 (you can check your version with the code in the misc dir) 
I'll get around to adding more version on an as needed basis, feel free to 
harass me if things dont work.

The OpenSSL versions are also important, but from what I can tell they change 
their internal data structs less often.

######################################################
#            CONTINUED DEVELOPMENT
######################################################

java-shim: Attempts to provide extended support for CertShim in Java through
use of agent interface. See subdir readme. Only partially implemented.

 Authors:
   Jiajun Han <hanjiajun@ufl.edu>
   Qile Zhu <qile@cise.ufl.edu>

PolarSSL: We override polarssl API to do manditory verification for running 
applications using polarssl library.

 Authors:
   Zhengzheng Liu <zoeliu@ufl.edu> 
   Yufan Xu <xyf1991@ufl.edu>


######################################################
## NSS Additional Installation Instructions
######################################################

1. As there is a conflict between the declaration of variable  - uint64 in two of the included header files - 

a. gnutls_int.h
b. nspr/obsolete.h

we commented out one of the declarations in file b to workaround the conflict.

## Change Log

1. Makefile - added code to link nss and nspr libraries and include header files during compilation
2. sslshim.c - added code for hooking NSS library call as well performing additonal cerificate verification based on the policy configuration. Also contains code to extract SHA1 fingerprint from the certificate.
3. converge.c - added code to extract host and port information needed by convergence from the NSS specific NSPR file descriptor.
4. converge.h - added definitions of new functions defined in converge.c
5. install_deb_dependencies.sh - added dependencies for nss and nspr
6. Added the nspr include files folder in the certshim source code folder.
7. Added NSS client code folder (See below)


## TLS NSS client folder

NSS Client used to test successful hook of NSS certificate verification function call. (Make file included)